from peer_check import check_peer_ssl, save_peers_to_json


def main():
    with open('./data/peers.txt') as file:
        relevant_peers = []
        unique_lines = set(file.readlines())
        for line in unique_lines:
            peer = line.rstrip()
            print(peer)
            if check_peer_ssl(peer):
                relevant_peers.append(peer)

            save_peers_to_json('list1.json', relevant_peers)


if __name__ == '__main__':
    main()
